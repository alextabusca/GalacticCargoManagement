package com.alex.model;

import java.util.ArrayList;

public class Character {

    private int id;
    private String name;
    private ArrayList<String> shipType;

    public Character(int id, String name, ArrayList<String> shipType) {
        this.id = id;
        this.name = name;
        this.shipType = shipType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> shipType() {
        return shipType;
    }

    public void setShipType(ArrayList<String> shipType) {
        this.shipType = shipType;
    }

    public String toString() {
        return "Id: " + id + ", Name: " + name + ", ShipTypes: " + shipType;
    }
}
