package com.alex.model;

public class Planet {

    private int id;
    private int distance;
    private String name;

    public Planet(int id, String name, int distance) {
        this.id = id;
        this.distance = distance;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Id: " + id + ", Name: " + name + ", Distance: " + distance;
    }
}
