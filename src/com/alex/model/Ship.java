package com.alex.model;

public class Ship {

    private int id;
    private int maxCargoWeight;
    private String name;
    private int speed;
    private String shipType;

    public Ship(int id, String name, int speed, String shipType, int maxCargoWeight) {
        this.id = id;
        this.maxCargoWeight = maxCargoWeight;
        this.name = name;
        this.speed = speed;
        this.shipType = shipType;
    }

    public int getMaxCargoWeight() {
        return maxCargoWeight;
    }

    public void setMaxCargoWeight(int maxCargoWeight) {
        this.maxCargoWeight = maxCargoWeight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public String toString() {
        return "Id: " + id + ", Name: " + name + ", Speed: " + speed + ", Type: " + shipType + ", Max Cargo: " + maxCargoWeight;
    }
}
