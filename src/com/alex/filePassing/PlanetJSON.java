package com.alex.filePassing;

import com.alex.model.Planet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PlanetJSON {

    private ArrayList<Planet> planets = new ArrayList<Planet>();


    public PlanetJSON() {
        JSONArray a;
        JSONParser parser = new JSONParser();

        {
            try {
                a = (JSONArray) parser.parse(new FileReader("src/resources/json/planets.json"));

                for(Object o: a) {
                    JSONObject planet = (JSONObject) o;

                    long id = (long) planet.get("id");
                    //System.out.println(id);

                    String name = (String) planet.get("name");
                    //System.out.println(name);

                    long distance = (long) planet.get("distance");
                    //System.out.println(distance);

                    Planet newPlanet = new Planet(((int) id), name, (int) distance);
                    planets.add(newPlanet);
                   System.out.println(newPlanet.toString());

                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
