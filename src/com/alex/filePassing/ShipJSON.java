package com.alex.filePassing;

import com.alex.model.Ship;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ShipJSON {

    private ArrayList<Ship> ships = new ArrayList<Ship>();

    public ShipJSON() {

        JSONArray a;
        JSONParser parser = new JSONParser();

        {
            try {
                a = (JSONArray) parser.parse(new FileReader("src/resources/json/ships.json"));

                for (Object o : a) {
                    JSONObject ship = (JSONObject) o;

                    long id = (long) ship.get("id");
                    //System.out.println(id);

                    String name = (String) ship.get("name");
                    //System.out.println(name);

                    long speed = (long) ship.get("speed");
                    //System.out.println(speed);

                    String type = (String) ship.get("type");
                    //System.out.println(type);

                    long maxCargoWeight = (long) ship.get("maxCargoWeight");
                    //System.out.println(maxCargoWeight);

                    Ship newShip = new Ship(((int) id), name, (int) speed, type, (int) maxCargoWeight);
                    ships.add(newShip);
                    System.out.println(newShip.toString());
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void listShips() {
        System.out.println(ships.toString());
    }
}
