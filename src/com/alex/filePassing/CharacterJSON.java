package com.alex.filePassing;

import com.alex.model.Character;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CharacterJSON {

    private ArrayList<Character> characters = new ArrayList<Character>();

    public CharacterJSON() {
        JSONArray a;
        JSONParser parser = new JSONParser();

        {
            try {
                a = (JSONArray) parser.parse(new FileReader("src/resources/json/characters.json"));

                for (Object o : a) {
                    JSONObject character = (JSONObject) o;

                    long id = (long) character.get("id");
                    //System.out.println(id);

                    String name = (String) character.get("name");
                    //System.out.println(name);

                    //String shipType = (String) character.get("shipsType");
                    JSONArray shipType = (JSONArray) character.get("shipsType");
                    /*for (Object s : shipType) {
                        System.out.println(s + " ");
                    }*/

                    Character newCharacter = new Character((int) id, name, shipType);
                    characters.add(newCharacter);
                    System.out.println(newCharacter.toString());

                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
