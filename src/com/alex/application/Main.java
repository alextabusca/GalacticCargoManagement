package com.alex.application;

import com.alex.filePassing.CharacterJSON;
import com.alex.filePassing.PlanetJSON;
import com.alex.filePassing.ShipJSON;

public class Main {

    public static void main(String[] args) {

        /*
            Update 1: So far I've managed to implement the parsing of the JSON files. Since I've never worked with this kind of file input before I had some trouble getting the code to work properly. Also,you may have to change the classpath to the respective json files - solved

            Update 2: Managed to get the info from the JSON and to create objects corresponding to the respective data
         */

        CharacterJSON testCharacter = new CharacterJSON();
        System.out.println();
        PlanetJSON testPlanet = new PlanetJSON();
        System.out.println();
        ShipJSON testShip = new ShipJSON();
    }
}
